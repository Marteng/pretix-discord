# Pretix x Discord

This simple PHP tool sends a message to a Discord webhook when Pretix registers an action. This could be something like a new order or an added event.

## Installation and configuration
You need to run `composer install` in order to initialize the project.
In the `/src` directory you will find a file named `hook.php`. Your domain should point to this directory. The parent directory should not be accessible.

Add a webhook to your Discord server and copy the URL. Rename `.env.example` to `.env` and change the `DISCORD_WEBHOOK` according to your copied URL. 

Add a webhook to Pretix according to [this](https://docs.pretix.eu/en/latest/api/webhooks.html) page. (e.g `https://example.org/hook.php`)

## Customization
You can customize `event_names` according to your language. Feel free to add your localization files via pull request (e.g `event_names_ger.json`).

## Security
Currently, the webhook script is accessible for everyone. This leads to the fact that anyone could possibly send a POST request to your server, thus sending you a lot of messages to your discord server. However, no personal data can be revealed.

Basic authentication will be added in future versions. 
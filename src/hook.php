<?php

use Dotenv\Dotenv;
use Woeler\DiscordPhp\Message\DiscordTextMessage;
use Woeler\DiscordPhp\Webhook\DiscordWebhook;

require dirname(__DIR__) . '/vendor/autoload.php';

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
$dotenv->required('DISCORD_WEBHOOK');

$webhook_url = $_ENV['DISCORD_WEBHOOK'];

$input = @file_get_contents('php://input');
$event = json_decode($input);

if(!isset($event->action)) {
    http_response_code(400);
    die();
}

$event_names = json_decode(file_get_contents(dirname(__DIR__).'/event_names.json'), true);
$name;
if(!isset($event_names[$event->action])) {
    $name = $event_names['unknown'];
} else {
    $name = $event_names[$event->action];
}


$message = (new DiscordTextMessage())
    ->setUsername('PretixBot')
    ->setContent('['.$event->organizer.' : '.$event->event.'] New pretix event: '. $name);

$webhook = new DiscordWebhook($webhook_url);
$webhook->send($message);